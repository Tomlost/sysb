package map.demo.controller;

import map.demo.entity.Admin;
import map.demo.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/map/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @RequestMapping("/adminLogin")
    public boolean adminLogin(String adminName, String adminPwd,HttpSession session){
        if (adminService.loginAdmin(adminName,adminPwd)!=null){
           session.setAttribute("admin",adminName);

            return true;
        }
        return false;
    }
    @RequestMapping("/getUserInfo")
    public String getUserInfo(HttpSession session){
        // 从Session获取登录用户
        String admin = (String) session.getAttribute("admin");


        return admin;
    }


    @RequestMapping("/loginOut")
    public void loginOut(HttpSession session) {

        session.removeAttribute("admin");

    }
}
