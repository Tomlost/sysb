package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Charge;
import map.demo.entity.Complain;
import map.demo.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nhxiaoqu/char")
public class ChargeController {
    @Autowired
    private ChargeService chargeService;

    @RequestMapping("/getAllChar")
    public PageInfo<Charge> getAllCom(int pageNum, int pageSize){
        return chargeService.getAllCharge(pageNum,pageSize);
    }

    @RequestMapping("/addChar")
    public boolean getAllCom(Charge charge){
        return chargeService.addCharge(charge);
    }
}
