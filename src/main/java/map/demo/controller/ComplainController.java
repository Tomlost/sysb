package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Complain;
import map.demo.service.ComplainService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/nhxiaoqu/com")
public class ComplainController {
    @Autowired
    private ComplainService complainService;

    @RequestMapping("/AllCom")
    public PageInfo<Complain> AllCom(int pageNum, int pageSize){
        return complainService.AllCom(pageNum,pageSize);
    }

    @RequestMapping("/getAllCom")
    public PageInfo<Complain> getAllCom(int pageNum, int pageSize,int userId ){
        return complainService.getAllCom(pageNum,pageSize,userId);
    }

    @RequestMapping("/getNoCom")
    public PageInfo<Complain> getNoCom(int pageNum, int pageSize,int userId){
        return complainService.getNoCom(pageNum,pageSize,userId);
    }

    @RequestMapping("/getYesCom")
    public PageInfo<Complain> getYesCom(int pageNum, int pageSize,int userId){
        return complainService.getYesCom(pageNum,pageSize,userId);
    }

    @PostMapping("/addCom")
    public boolean addCom(Complain complain, @RequestParam("file") MultipartFile file, HttpServletRequest request)throws IOException{
        String oldFileName = file.getOriginalFilename();
        String newFileName = UUID.randomUUID().toString()+"."+ FilenameUtils.getExtension(oldFileName);

        String path = request.getServletContext().getRealPath("static"+ File.separator+"upload");
        File temp = new File(path);
        if (!temp.exists()){
            temp.mkdirs();
        }

        try {
            file.transferTo(new File(path,newFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        complain.setComplainPath(newFileName);
        complain.setComplainDel(0);

        if (complainService.addCom(complain)){
            return true;
        }
        return false;
    }

    @RequestMapping("/showCom")
        public Complain showCom(int complainId){
        return complainService.showCom(complainId);
    }

    @RequestMapping("/updateCom")
    public boolean updateCom(Integer complainId){
        return complainService.update(complainId);
    }
}
