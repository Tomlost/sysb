package map.demo.controller;

import map.demo.dao.NewsMapper;
import map.demo.dao.OrderMapper;
import map.demo.entity.User;
import map.demo.entity.News;
import map.demo.entity.Order;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.web.bind.annotation.*;
import map.demo.dao.UserMapper;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.Console;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * 2023.11.11
 * @author
 */
@RestController
@RequestMapping("/excel")
public class ExcelController {
    @Resource
    private NewsMapper newsMapper;
    @Resource
    private OrderMapper orderMapper;
   // private UserMapper userMapper;
    //@RequestMapping(value = "UserExcelDownloads", method = RequestMethod.GET)
  // public void downloadAllClassmate(String inputcontent1,HttpServletResponse response) throws IOException {
   @RequestMapping(value = "UserExcelDownloads",method = RequestMethod.GET)
    public void downloadAllClassmate(@RequestParam(value="inputcontent1", required=false) String inputcontent1 ,HttpServletResponse response) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("信息表");
        List<News> newsList;

        //List<User> userList = userService.queryAll(new User());
        //List<User> userList = userMapper.showUser();

       //String inputcontent=inputcontent1;

        System.out.println("neirong" + inputcontent1);
        if(inputcontent1==null || inputcontent1.isEmpty()){
             newsList= newsMapper.getAllNewsList();
        }
         else{
            newsList= newsMapper.showNewsBytitle(inputcontent1);
            System.out.println("到了neirong" + inputcontent1);
        }

       // newsList= newsMapper.getAllNewsList();
        String fileName = "newsinf"  + ".xls";//设置要导出的文件的名字
        //新增数据行，并且设置单元格数据
       System.out.println("到了neirong" + fileName);
        int rowNum = 1;

        String[] headers = { "标题", "创建者", "创建时间"};
        //headers表示excel表中第一行的表头

        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头

        for(int i=0;i<headers.length;i++){
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }

        //在表中存放查询到的数据放入对应的列
        for (News news : newsList) {
            HSSFRow row1 = sheet.createRow(rowNum);
            row1.createCell(0).setCellValue(news.getTitle());
            row1.createCell(1).setCellValue(news.getAdder());
            row1.createCell(2).setCellValue(news.getAddtime());
            //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //String dateString = formatter.format(news.getAddtime());
            //row1.createCell(3).setCellValue(dateString);
           // System.out.println(dateString);
            rowNum++;
        }

        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        response.flushBuffer();
        workbook.write(response.getOutputStream());
    }
    @RequestMapping(value = "OderwclExcelDownloads",method = RequestMethod.GET)
    public void downloadAllClassmate(HttpServletResponse response) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("信息表");
        List<Order> orderList = orderMapper.getAllwclOrder();
        String fileName = "oderwclinf"  + ".xls";//设置要导出的文件的名字
        //新增数据行，并且设置单元格数据

        int rowNum = 1;

        String[] headers = { "微信标识", "联系电话", "预约时间","预约类别", "站点名称", "预约状态"};
        //headers表示excel表中第一行的表头

        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头

        for(int i=0;i<headers.length;i++){
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }

        //在表中存放查询到的数据放入对应的列
        for (Order oders : orderList) {
            HSSFRow row1 = sheet.createRow(rowNum);
            row1.createCell(0).setCellValue(oders.getOpenId());
            row1.createCell(1).setCellValue(oders.getoPhone());
            //row1.createCell(2).setCellValue(oders.getoDate());
            row1.createCell(3).setCellValue(oders.getyName());
            row1.createCell(4).setCellValue(oders.getzName());
            row1.createCell(5).setCellValue(oders.getoSituation());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(oders.getoDate());
            row1.createCell(2).setCellValue(dateString);

            rowNum++;
        }

        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        response.flushBuffer();
        workbook.write(response.getOutputStream());
    }
}
