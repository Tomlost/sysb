package map.demo.controller;
import com.github.pagehelper.PageInfo;
import map.demo.entity.Admin;
import map.demo.entity.News;

import map.demo.service.NewsService;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.plugin.dom.core.Element;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/map/news")
public class GetallNewsController {
    @Autowired
    private NewsService newsService;

    @RequestMapping("/getAllNewsList")
    public PageInfo<News> getAllNewsList(int pageNum, int pageSize){
        return newsService.getAllNewsList(pageNum,pageSize);
    }

    @RequestMapping("/getAllNewsListByZd")
    public PageInfo<News> getAllNewsListByZd(){
        return newsService.getAllNewsListByZd(5,1);
    }

    @RequestMapping("/getAllNewsListByFenLei")
    public PageInfo<News> getAllNewsListByFenLei(News news,int fl){
        if (fl == 0){
            news.setGjz("信贷服务");
        }else if (fl == 1){
            news.setGjz("现金服务");
        }else if (fl == 2){
            news.setGjz("反假货币");
        }else if (fl == 3){
            news.setGjz("国债下乡");
        }else if (fl == 4){
            news.setGjz("助农取款");
        }else if (fl == 5){
            news.setGjz("征信服务");
        }

        return newsService.getAllNewsListByFenLei(1000,1,news);
    }

    @RequestMapping("/showNewsById")
    public News showNewsById(int id){
        return newsService.showNewsById(id);
    }

    @RequestMapping("/showNewsBytitle")
    public PageInfo<News> showNewsBytitle(int pageNum, int pageSize,String title){

        return newsService.showNewsBytitle(pageNum,pageSize,title);
    }
    @RequestMapping("/updateNewsInfo")
    public boolean updateNewsInfo(News news){
        return newsService.updateNewsInfo(news);
    }

    @RequestMapping("/addNews")


    public boolean addNews(News news ){


        String addtime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        System.out.println(addtime);

        news.setVisit(0);
        news.setIsVisible(0);
        news.setZhiding(0);
        news.setAddtime(addtime);

        return newsService.addNews(news);
    }



    @RequestMapping("/deleteNews")
    public boolean deleteNews(int id){
        return newsService.deleteNews(id);
    }

    @RequestMapping("/editzhiding")
    public boolean editzhiding(int id){
        return newsService.editzhiding(id);
    }

    @RequestMapping("/editnozhiding")
    public boolean editnozhiding(int id){
        return newsService.editnozhiding(id);
    }

    @RequestMapping("/editVisible")
    public boolean editVisible(int id){
        return newsService.editVisible(id);
    }

    @RequestMapping("/editnoVisible")
    public boolean editnoVisible(int id){
        return newsService.editnoVisible(id);
    }


}
