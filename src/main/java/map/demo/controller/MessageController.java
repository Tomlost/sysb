package map.demo.controller;

import map.demo.entity.Message;
import map.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/map/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @RequestMapping("/getMessageList")
    public List<Message> getMessageList(){
        return messageService.getMessageList();
    }

    @RequestMapping("/getMessageByCid")
    public Message getMessageByCid(int cid) {
        return messageService.getMessageByCid(cid);
    }

    @RequestMapping("/getMessageListByPower")
    public List<Message> getMessageListByPower(int zPower){
        if (zPower<4){
            return messageService.getMessageList();
        }else {
            return messageService.getMessageListByPower();
        }

    }
}
