package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Notice;
import map.demo.service.NoticeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/nhxiaoqu/notice")
public class NoticeController {
    @Autowired
    private NoticeService noticeService;

    @RequestMapping("showNotice")
    public PageInfo<Notice> showNotice(int pageNum, int pageSize){
       return noticeService.showAllNotice(pageNum,pageSize);
    }

    @RequestMapping("show")
    public Notice show(int id){

        return noticeService.show(id);
    }
}
