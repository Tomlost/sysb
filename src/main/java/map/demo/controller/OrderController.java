package map.demo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import map.demo.entity.Order;
import map.demo.service.OrderService;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/map/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping("/getOrderByOpenId")
    public PageInfo<Order> getOrderByOpenId(String openId) {
        return orderService.getOrderByOpenId(100,1,openId);
    }

    @RequestMapping("/getAllOrderList")
    public PageInfo<Order> getAllOrderList(int pageNum, int pageSize) {
        return orderService.getAllOrderList(pageNum,pageSize);
    }
    @RequestMapping("/getAllOrder")
    public PageInfo<Order> getAllOrder(int pageNum, int pageSize) {
        return orderService.getAllOrder(pageNum,pageSize);
    }

    @RequestMapping("/getAllwclOrder")
    public PageInfo<Order> getAllwclOrder(int pageNum, int pageSize) {
        return orderService.getAllwclOrder(pageNum,pageSize);
    }
    @RequestMapping("/getOrderByTid")
    public PageInfo<Order> getOrderByTid(int tid) {
        return orderService.getOrderByTid(100,1,tid);
    }
    @RequestMapping("/updateoder")
    public boolean updateoder(Integer oid){
        return orderService.updateoder(oid);
    }

    @RequestMapping("/addOrder")
    public boolean addOrder(Order order,String Date1){
        System.out.println("oldData:" + Date1);

        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(Date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("data:" + date);
        order.setoDate(date);

        return orderService.addOrder(order);
    }

    @RequestMapping("/delOrder")
    public boolean delOrder(@Param("oid")int oid, @Param("oSituation")String oSituation) {
        return orderService.delOrder(oid,oSituation);
    }



    @RequestMapping("/testopenid")
    public String getUserInfo(@RequestParam(name = "code") String code) throws Exception {
        System.out.println("code:" + code);
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        url += "?appid=wx18978a982a303022";//自己的appid
        url += "&secret=6cbb970cfe13ca1608b6d06c5fa86383";//自己的appSecret
        url += "&js_code=" + code;
        url += "&grant_type=authorization_code";
        url += "&connect_redirect=1";
        String res = null;
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        // DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);    //GET方式
        CloseableHttpResponse response = null;
        // 配置信息
        RequestConfig requestConfig = RequestConfig.custom()          // 设置连接超时时间(单位毫秒)
                .setConnectTimeout(5000)                    // 设置请求超时时间(单位毫秒)
                .setConnectionRequestTimeout(5000)             // socket读写超时时间(单位毫秒)
                .setSocketTimeout(5000)                    // 设置是否允许重定向(默认为true)
                .setRedirectsEnabled(false).build();           // 将上面的配置信息 运用到这个Get请求里
        httpget.setConfig(requestConfig);                         // 由客户端执行(发送)Get请求
        response = httpClient.execute(httpget);                   // 从响应模型中获取响应实体
        HttpEntity responseEntity = response.getEntity();
//        System.out.println("响应状态为:" + response.getStatusLine());
        if (responseEntity != null) {
            res = EntityUtils.toString(responseEntity);
//            System.out.println("响应内容长度为:" + responseEntity.getContentLength());
//            System.out.println("响应内容为:" + res);
        }
        // 释放资源
        if (httpClient != null) {
            httpClient.close();
        }
        if (response != null) {
            response.close();
        }
        JSONObject jo = JSON.parseObject(res);
        String openid = jo.getString("openid");
        System.out.println("openid:" + openid);
        return openid;
    }


    /**
     * 获取手机号
     */
//    @RequestMapping("/getPhoneNumber")
//    public String getPhoneNumber(@RequestParam(name = "code") String code) throws Exception {
//        System.out.println("code:" + code);
//        String url = "https://api.weixin.qq.com/sns/jscode2session";
//        url += "?appid=wx3de71da1411015dd";//自己的appid
//        url += "&secret=5cf182467283379884878146093a5333";//自己的appSecret
//        url += "&js_code=" + code;
//        url += "&grant_type=authorization_code";
//        url += "&connect_redirect=1";
//        String res = null;
//        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        // DefaultHttpClient();
//        HttpGet httpget = new HttpGet(url);    //GET方式
//        CloseableHttpResponse response = null;
//        // 配置信息
//        RequestConfig requestConfig = RequestConfig.custom()          // 设置连接超时时间(单位毫秒)
//                .setConnectTimeout(5000)                    // 设置请求超时时间(单位毫秒)
//                .setConnectionRequestTimeout(5000)             // socket读写超时时间(单位毫秒)
//                .setSocketTimeout(5000)                    // 设置是否允许重定向(默认为true)
//                .setRedirectsEnabled(false).build();           // 将上面的配置信息 运用到这个Get请求里
//        httpget.setConfig(requestConfig);                         // 由客户端执行(发送)Get请求
//        response = httpClient.execute(httpget);                   // 从响应模型中获取响应实体
//        HttpEntity responseEntity = response.getEntity();
////        System.out.println("响应状态为:" + response.getStatusLine());
//        if (responseEntity != null) {
//            res = EntityUtils.toString(responseEntity);
////            System.out.println("响应内容长度为:" + responseEntity.getContentLength());
////            System.out.println("响应内容为:" + res);
//        }
//        // 释放资源
//        if (httpClient != null) {
//            httpClient.close();
//        }
//        if (response != null) {
//            response.close();
//        }
//        JSONObject jo = JSON.parseObject(res);
//        String phoneNumber = jo.getString("phoneNumber");
//        System.out.println("phoneNumber:" + phoneNumber);
//
//        return phoneNumber;
//    }
}
