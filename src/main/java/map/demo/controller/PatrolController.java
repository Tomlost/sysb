package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Patrol;
import map.demo.service.PatrolService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/map/pat")
public class PatrolController {
    @Autowired
    private PatrolService patrolService;

    @RequestMapping("/addPatrol")
    public boolean addPatrol(Patrol patrol, @RequestParam("file") MultipartFile file, HttpServletRequest request)throws IOException {
        String oldFileName = file.getOriginalFilename();
        String newFileName = UUID.randomUUID().toString()+"."+ FilenameUtils.getExtension(oldFileName);

        String path = request.getServletContext().getRealPath("static"+ File.separator+"upload");
        File temp = new File(path);
        if (!temp.exists()){
            temp.mkdirs();
        }

        try {
            file.transferTo(new File(path,newFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Date date = new Date();

        patrol.setPimg(newFileName);
        patrol.setPdate(date);

        return patrolService.addPatrol(patrol);
    }

    @RequestMapping("/getAllPatrol")
    public PageInfo<Patrol> getAllPatrol(int pageNum, int pageSize) {
        return patrolService.getAllPatrol(pageNum,pageSize);
    }

    @RequestMapping("/delPatrol")
    public boolean delPatrol(int pid) {
        return patrolService.delPatrol(pid);
    }
}
