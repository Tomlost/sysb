package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Pfenlei;
import map.demo.entity.Product;
import map.demo.service.PfenleiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/map/pfl")
public class PfenleiController {

    @Autowired
    private PfenleiService pfenleiService;

    @RequestMapping("/getAllPf")
    public List<Pfenlei> getAllPf() {
        return pfenleiService.getAllPf();
    }

}
