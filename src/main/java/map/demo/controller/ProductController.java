package map.demo.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.entity.Product;
import map.demo.service.PatrolService;
import map.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/map/pro")
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/getAllProByFl")
    public PageInfo<Product> getAllProByFl(Product product) {
        product.setFlid(product.getFlid()+1);
        return productService.getAllProByFl(product);
    }

    @RequestMapping("/getProByPid")
    public Product getProByPid(int pid) {
        return productService.getProByPid(pid);
    }
}
