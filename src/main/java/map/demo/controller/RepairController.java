package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Repair;
import map.demo.entity.User;
import map.demo.service.RepairService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/nhxiaoqu/repair")
public class RepairController {
    @Autowired
    private RepairService repairService;

    @RequestMapping("/addRepair")
    public boolean addRepair(Repair repair, @RequestParam("file") MultipartFile file, HttpServletRequest request){
        String oldFileName = file.getOriginalFilename();
        String newFileName = UUID.randomUUID().toString()+"."+ FilenameUtils.getExtension(oldFileName);

        String path = request.getServletContext().getRealPath("static"+ File.separator+"upload");
        File temp = new File(path);
        if (!temp.exists()){
            temp.mkdirs();
        }

        try {
            file.transferTo(new File(path,newFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        repair.setRepairDel(0);
        repair.setRepairPath(newFileName);

        if (repairService.addRepair(repair)){
            return true;
        }
        return false;
    }

    @RequestMapping("/showR")
    public PageInfo<Repair> showR(int pageNum, int pageSize){
        return repairService.showR(pageNum,pageSize);
    }

    @RequestMapping("/updateRepair")
    public boolean updateRepair(Repair repair){
        return repairService.updateRepair(repair);
    }
}
