package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Repair;
import map.demo.entity.Room;
import map.demo.service.RoomService;
import map.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nhxiaoqu/room")
public class RoomController {
    @Autowired
    private RoomService roomService;

    @Autowired
    private UserService userService;

    @RequestMapping("/allRoom")
    public PageInfo<Room> allRoom(int pageNum, int pageSize){
        return roomService.allRoom(0,pageNum,pageSize);
    }

    @RequestMapping("/updateRoom")
    public boolean updateRoom(Room room){
        userService.updateUserState(room.getUid());
        return roomService.updateRoom(room.getUid(),room.getRid());
    }
}
