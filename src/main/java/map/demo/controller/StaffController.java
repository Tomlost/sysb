package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Staff;
import map.demo.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nhxiaoqu/staff")
public class StaffController {
    @Autowired
    private StaffService staffService;


    @RequestMapping("/staffAll")
    public PageInfo<Staff> staffAll(int pageSize,int pageNum){
        return staffService.staffAll(pageSize,pageNum);
    }

    @RequestMapping("/deleteStaff")
    public boolean deleteStaff(int staffId){
        return staffService.deleteStaff(staffId);
    }


    @RequestMapping("/addStaff")
    public boolean addStaff(Staff staff){
        staff.setPositionId(1);
        staff.setStaffDel("0");
        return staffService.addStaff(staff);
    }
}
