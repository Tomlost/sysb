package map.demo.controller;

import map.demo.entity.Stati;
import map.demo.service.StatiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/map/sta")
public class StatiController {
    @Autowired
    private StatiService statiService;

    @RequestMapping("/getAllSta")
    public Stati getAllSta() {
        return statiService.getAllSta();
    }

    @RequestMapping("/upYySum")
    public boolean upYySum() {
        return statiService.upYySum();
    }

    @RequestMapping("/upYwSum")
    public boolean upYwSum() {
        return statiService.upYwSum();
    }

    @RequestMapping("/upZdSum")
    public boolean upZdSum() {
        return statiService.upZdSum();
    }

    @RequestMapping("/upFwSum")
    public boolean upFwSum() {
        return statiService.upFwSum();
    }

}
