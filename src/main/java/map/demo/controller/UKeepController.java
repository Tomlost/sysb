package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.UKeep;
import map.demo.service.UKeepService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/map/keep")
public class UKeepController {
    @Autowired
    private UKeepService uKeepService;

    @RequestMapping("/addUKeep")
    public boolean addUKeep(UKeep uKeep, @RequestParam("file") MultipartFile file, HttpServletRequest request)throws IOException{
        String oldFileName = file.getOriginalFilename();
        String newFileName = UUID.randomUUID().toString()+"."+ FilenameUtils.getExtension(oldFileName);

        String path = request.getServletContext().getRealPath("static"+ File.separator+"upload");
        File temp = new File(path);
        if (!temp.exists()){
            temp.mkdirs();
        }

        try {
            file.transferTo(new File(path,newFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Date date = new Date();

        uKeep.setUimg(newFileName);
        uKeep.setUdate(date);

        return uKeepService.addUKeep(uKeep);
    }

    @RequestMapping("/getAllUKeep")
    public PageInfo<UKeep> getAllUKeep(int pageNum, int pageSize) {
        return uKeepService.getAllUKeep(pageNum,pageSize);
    }

    @RequestMapping("/delUKeep")
    public boolean delUKeep(int uid) {
        return uKeepService.delUKeep(uid);
    }
}
