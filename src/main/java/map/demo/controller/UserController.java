package map.demo.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import map.demo.entity.User;
import map.demo.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/nhxiaoqu/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/update")
    public boolean update(String userPwd,Integer userId){
         if (userService.update(userPwd,userId)){
            return true;
        }else {
            return false;
        }
    }

    @RequestMapping("/my")
    public User my(String phone,String password){
        User user = userService.login(phone,password);
        if (user==null){
            return null;
        }else {
            return user;
        }
    }

    @RequestMapping("/showUser")
    public PageInfo<User> showUser(int pageNum,int pageSize){
        return userService.userShow(pageNum,pageSize);
    }

    @RequestMapping("/showUserById")
    public User showUserById(int userId){
        return userService.showUserById(userId);
    }

    @RequestMapping("/updateUser")
    public boolean updateUser(User user){
        return userService.updateUser(user);
    }

    @RequestMapping("/addUser")
    public boolean addUser(User user){
        return userService.addUser(user);
    }

    @RequestMapping("/deleteUser")
    public boolean deleteUser(int userId){
        return userService.deleteUser(userId);
    }

    @RequestMapping("/showUserState")
    public List<User> showUserState(){
        return userService.showUserState();
    }

    @RequestMapping("/updateUserState")
    public boolean updateUserState(int userId){
        return userService.updateUserState(userId);
    }
}
