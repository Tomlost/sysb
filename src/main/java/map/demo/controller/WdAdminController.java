package map.demo.controller;

import com.github.pagehelper.PageInfo;
import map.demo.entity.WdAdmin;
import map.demo.service.WdAdminService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/map/wdAdmin")
public class WdAdminController {
    @Autowired
    private WdAdminService wdAdminService;

    @RequestMapping("/loginWdAdmin")
    public WdAdmin loginWdAdmin(@Param("wphone")String wphone) {
        return wdAdminService.loginWdAdmin(wphone);
    }

    @RequestMapping("/getAllWdAdminList")
    public PageInfo<WdAdmin> getAllWdAdminList(int pageNum, int pageSize) {
        return wdAdminService.getAllWdAdminList(pageNum,pageSize);
    }

    @RequestMapping("/getWdAdminListByZid")
    public PageInfo<WdAdmin> getWdAdminListByZid(int pageNum,int pageSize,int zid) {
        return wdAdminService.getWdAdminListByZid(pageNum,pageSize,zid);
    }

    @RequestMapping("/addWdAdmin")
    public boolean addWdAdmin(WdAdmin wdAdmin) {
        return wdAdminService.addWdAdmin(wdAdmin);
    }

    @RequestMapping("/delWdAdmin")
    public boolean delWdAdmin(int wid) {
        return wdAdminService.delWdAdmin(wid);
    }

}
