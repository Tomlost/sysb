package map.demo.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class WelcomeController {
    @RequestMapping("/")
    public String view() {
        return "forward:login.html";}
}
