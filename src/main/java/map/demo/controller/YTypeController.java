package map.demo.controller;

import map.demo.entity.YType;
import map.demo.service.YTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/map/yType")
public class YTypeController {
    @Autowired
    private YTypeService yTypeService;

    @RequestMapping("/getYTypeByYid")
    public YType getYTypeByYid(int yid) {
        return yTypeService.getYTypeByYid(yid);
    }


    @RequestMapping("/getAllYTypeList")
    public List<YType> getAllYTypeList() {
        return yTypeService.getAllYTypeList();
    }
}
