package map.demo.controller;

import map.demo.entity.Ysamll;
import map.demo.service.YsamllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/map/ys")
public class YsamllController {
    @Autowired
    private YsamllService ysamllService;

    @RequestMapping("/getAllYs")
    public List<Ysamll> getAllYs() {
        return ysamllService.getAllYs();
    }

    @RequestMapping("/updateYs")
    @ResponseBody
    public boolean updateYs(Ysamll ysamll) {
        return ysamllService.updateYs(ysamll);
    }
}
