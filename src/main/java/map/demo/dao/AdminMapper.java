package map.demo.dao;

import map.demo.entity.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface AdminMapper {
    Admin loginAdmin(@Param("adminName") String adminName, @Param("adminPwd") String adminPwd);
}
