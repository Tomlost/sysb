package map.demo.dao;

import map.demo.entity.Charge;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ChargeMapper {
    List<Charge> getAllCharge();

    int addCharge(Charge charge);
}
