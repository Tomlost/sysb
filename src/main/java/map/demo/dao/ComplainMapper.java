package map.demo.dao;

import map.demo.entity.Complain;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ComplainMapper {
    List<Complain> AllCom();

    List<Complain> getAllCom(int userId);

    List<Complain> getNoCom(int userId);

    List<Complain> getYesCom(int userId);

    int addCom(Complain complain);

    Complain showCom(int complainId);

    int updateCom(int complainId);
}
