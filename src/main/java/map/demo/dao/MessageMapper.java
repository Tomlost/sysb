package map.demo.dao;

import map.demo.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MessageMapper {
    List<Message> getMessageList();

    Message getMessageByCid(int cid);

    List<Message> getMessageListByPower();
}
