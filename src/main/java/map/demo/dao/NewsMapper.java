package map.demo.dao;
import map.demo.entity.Message;
import map.demo.entity.News;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.apache.ibatis.annotations.Param;
import java.util.List;
@Mapper
@Repository
public interface NewsMapper {

    List<News> getAllNewsList();

    List<News> getAllNewsListByFenLei(News news);

    List<News> getAllNewsListByZd();

    News showNewsById(int id);

    int updateNewsInfo(News news);

    int addNews(News news);

    int deleteNews(int id);

    int editzhiding(int id);

    int editnozhiding(int id);

    int editVisible(int id);

    int editnoVisible(int id);

    List<News> showNewsBytitle(String title);
}

