package map.demo.dao;

import map.demo.entity.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Mapper
@Repository
public interface NoticeMapper {
    List<Notice> getNoticeList();

    Notice getNotice(int id);
}
