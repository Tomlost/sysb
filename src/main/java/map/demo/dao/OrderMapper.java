package map.demo.dao;

import map.demo.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderMapper {
    List<Order> getAllOrderList();
    List<Order> getAllOrder();
    List<Order> getAllwclOrder();

    List<Order> getOrderByTid(int tid);

    int addOrder(Order order);
    int updateoder(int oid);
    int delOrder(@Param("oid")int oid, @Param("oSituation")String oSituation);

    List<Order> getOrderByOpenId(String openId);

}
