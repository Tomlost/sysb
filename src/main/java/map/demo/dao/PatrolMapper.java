package map.demo.dao;

import map.demo.entity.Patrol;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PatrolMapper {
    int addPatrol(Patrol patrol);

    List<Patrol> getAllPatrol();

    int delPatrol(int pid);
}
