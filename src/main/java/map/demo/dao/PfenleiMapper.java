package map.demo.dao;

import map.demo.entity.Pfenlei;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PfenleiMapper {
    List<Pfenlei> getAllPf();
}
