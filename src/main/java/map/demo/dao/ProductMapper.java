package map.demo.dao;

import map.demo.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductMapper {
    List<Product> getAllProByFl(Product product);

    Product getProByPid(int pid);

}
