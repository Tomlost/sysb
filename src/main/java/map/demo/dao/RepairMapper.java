package map.demo.dao;

import map.demo.entity.Repair;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RepairMapper {
    int addRepair(Repair repair);

    int updateRepair(Repair repair);

    List<Repair> showR();
}
