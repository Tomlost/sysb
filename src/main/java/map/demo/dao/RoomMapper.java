package map.demo.dao;

import map.demo.entity.Room;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface RoomMapper {
    List<Room> allRoom(int uid);

    int updateRoom(@Param("uid")int uid, @Param("id")int id);
}
