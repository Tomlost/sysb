package map.demo.dao;

import map.demo.entity.Staff;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StaffMapper {
    List<Staff> staffAll();

    int updateStaff(Staff staff);

    int addStaff(Staff staff);

    int deleteStaff(int staffId);

    Staff showStaff(int staffId);
}
