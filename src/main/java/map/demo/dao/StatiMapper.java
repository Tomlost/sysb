package map.demo.dao;

import map.demo.entity.Stati;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StatiMapper {
    Stati getAllSta();

    int upYySum();

    int upYwSum();

    int upZdSum();

    int upFwSum();
}
