package map.demo.dao;

import map.demo.entity.UKeep;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UKeepMapper {
    int addUKeep(UKeep uKeep);

    List<UKeep> getAllUKeep();

    int delUKeep(int uid);
}
