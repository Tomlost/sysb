package map.demo.dao;

import map.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    User login(@Param("userTel") String userTel, @Param("userPwd") String userPwd);

    int update(@Param("userPwd")String userPwd,@Param("userId")Integer userId);

    List<User> showUser();

    List<User> showUserState();

    int updateUserState(int userId);

    User showUserById(int userId);

    int updateUser(User user);

    int addUser(User user);

    int deleteUser(int userId);
}
