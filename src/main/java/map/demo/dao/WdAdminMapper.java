package map.demo.dao;

import map.demo.entity.WdAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface WdAdminMapper {

    //登陆
    WdAdmin loginWdAdmin(@Param("wphone")String wphone);

    List<WdAdmin> getAllWdAdminList();

    List<WdAdmin> getWdAdminListByZid(int zid);


    int addWdAdmin(WdAdmin wdAdmin);

    int delWdAdmin(int wid);


}
