package map.demo.dao;

import map.demo.entity.YType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface YTypeMapper {
    YType getYTypeByYid(int yid);

    List<YType> getAllYTypeList();
}
