package map.demo.dao;

import map.demo.entity.Ysamll;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface YsamllMapper {
    List<Ysamll> getAllYs();

    int updateYs(Ysamll ysamll);
}
