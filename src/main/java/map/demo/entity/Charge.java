package map.demo.entity;

import java.util.Date;

public class Charge {
    private int chargeId;
    private Date chargeTime;
    private int chargeMoney;
    private String chargeDel;
    private String uname;
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public int getChargeId() {
        return chargeId;
    }

    public void setChargeId(int chargeId) {
        this.chargeId = chargeId;
    }

    public Date getChargeTime() {
        return chargeTime;
    }

    public void setChargeTime(Date chargeTime) {
        this.chargeTime = chargeTime;
    }

    public int getChargeMoney() {
        return chargeMoney;
    }

    public void setChargeMoney(int chargeMoney) {
        this.chargeMoney = chargeMoney;
    }

    public String getChargeDel() {
        return chargeDel;
    }

    public void setChargeDel(String chargeDel) {
        this.chargeDel = chargeDel;
    }
}
