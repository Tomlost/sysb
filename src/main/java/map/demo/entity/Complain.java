package map.demo.entity;

import java.util.Date;

public class Complain {
    private int complainId;
    private Date complainTime;
    private String complainInfo;
    private int userId;
    private int complainDel;
    private int complainDentify;
    private String complainPath;
    private String complainTitle;

    public int getComplainId() {
        return complainId;
    }

    public void setComplainId(int complainId) {
        this.complainId = complainId;
    }

    public Date getComplainTime() {
        return complainTime;
    }

    public void setComplainTime(Date complainTime) {
        this.complainTime = complainTime;
    }

    public String getComplainInfo() {
        return complainInfo;
    }

    public void setComplainInfo(String complainInfo) {
        this.complainInfo = complainInfo;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getComplainDel() {
        return complainDel;
    }

    public void setComplainDel(int complainDel) {
        this.complainDel = complainDel;
    }

    public int getComplainDentify() {
        return complainDentify;
    }

    public void setComplainDentify(int complainDentify) {
        this.complainDentify = complainDentify;
    }

    public String getComplainPath() {
        return complainPath;
    }

    public void setComplainPath(String complainPath) {
        this.complainPath = complainPath;
    }

    public String getComplainTitle() {
        return complainTitle;
    }

    public void setComplainTitle(String complainTitle) {
        this.complainTitle = complainTitle;
    }
}
