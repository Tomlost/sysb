package map.demo.entity;

public class Cpname {
    private int cid;            //id
    private String cpname;      //单位名称
    private String cpdetailes;  //单位电话地址。。
    private String cpbeizhu;    //暂无

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCpname() {
        return cpname;
    }

    public void setCpname(String cpname) {
        this.cpname = cpname;
    }

    public String getCpdetailes() {
        return cpdetailes;
    }

    public void setCpdetailes(String cpdetailes) {
        this.cpdetailes = cpdetailes;
    }

    public String getCpbeizhu() {
        return cpbeizhu;
    }

    public void setCpbeizhu(String cpbeizhu) {
        this.cpbeizhu = cpbeizhu;
    }
}
