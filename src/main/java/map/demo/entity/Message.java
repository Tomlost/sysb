package map.demo.entity;

public class Message {
    private int cid;
    private String zName;      //站点名字
    private String zPhone;     //电话
    private double longiTude;  //经度
    private double latiTude;   //纬度
    private String zPri;       //负责人
    private int zDelete;       //删除标识
    private String zAddress;   //地址
    private int zPower;        //权限

    public int getzPower() {
        return zPower;
    }

    public void setzPower(int zPower) {
        this.zPower = zPower;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getzName() {
        return zName;
    }

    public void setzName(String zName) {
        this.zName = zName;
    }

    public String getzPhone() {
        return zPhone;
    }

    public void setzPhone(String zPhone) {
        this.zPhone = zPhone;
    }

    public double getLongiTude() {
        return longiTude;
    }

    public void setLongiTude(double longiTude) {
        this.longiTude = longiTude;
    }

    public double getLatiTude() {
        return latiTude;
    }

    public void setLatiTude(double latiTude) {
        this.latiTude = latiTude;
    }

    public String getzPri() {
        return zPri;
    }

    public void setzPri(String zPri) {
        this.zPri = zPri;
    }

    public int getzDelete() {
        return zDelete;
    }

    public void setzDelete(int zDelete) {
        this.zDelete = zDelete;
    }

    public String getzAddress() {
        return zAddress;
    }

    public void setzAddress(String zAddress) {
        this.zAddress = zAddress;
    }
}
