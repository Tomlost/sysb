package map.demo.entity;

public class News {
    private int id;
    private String title;      //标题
    private String fenlei;     //分类
    private String content;  //内容
    private String addtime;   //时间
    private String adder;       //创建人
    private int visit;       //访问次数
    private String gjz;   //关键字


    private int zhiding;//是否置顶



    private int isVisible;//是否可见

    public int getZhiding() {
        return zhiding;
    }

    public void setZhiding(int zhiding) {
        this.zhiding = zhiding;
    }

    public int getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(int isVisible) {
        this.isVisible = isVisible;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFenlei() {
        return fenlei;
    }

    public void setFenlei(String fenlei) {
        this.fenlei = fenlei;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getAdder() {
        return adder;
    }

    public void setAdder(String adder) {
        this.adder = adder;
    }

    public int getVisit() {
        return visit;
    }

    public void setVisit(int visit) {
        this.visit = visit;
    }

    public String getGjz() {
        return gjz;
    }

    public void setGjz(String gjz) {
        this.gjz = gjz;
    }
}
