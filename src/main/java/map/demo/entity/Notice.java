package map.demo.entity;

public class Notice {
    private int noticeId;
    private String bianhao;
    private String noticeName;
    private String noticeOb;
    private String noticeSt;
    private String noticeEnd;
    private String noticeInfo;
    private String noticeDel;

    public int getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(int noticeId) {
        this.noticeId = noticeId;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao;
    }

    public String getNoticeName() {
        return noticeName;
    }

    public void setNoticeName(String noticeName) {
        this.noticeName = noticeName;
    }

    public String getNoticeOb() {
        return noticeOb;
    }

    public void setNoticeOb(String noticeOb) {
        this.noticeOb = noticeOb;
    }

    public String getNoticeSt() {
        return noticeSt;
    }

    public void setNoticeSt(String noticeSt) {
        this.noticeSt = noticeSt;
    }

    public String getNoticeEnd() {
        return noticeEnd;
    }

    public void setNoticeEnd(String noticeEnd) {
        this.noticeEnd = noticeEnd;
    }

    public String getNoticeInfo() {
        return noticeInfo;
    }

    public void setNoticeInfo(String noticeInfo) {
        this.noticeInfo = noticeInfo;
    }

    public String getNoticeDel() {
        return noticeDel;
    }

    public void setNoticeDel(String noticeDel) {
        this.noticeDel = noticeDel;
    }
}
