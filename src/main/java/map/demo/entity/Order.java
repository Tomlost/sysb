package map.demo.entity;

import java.util.Date;

public class Order {
    private int oid;                //id
    private int oType;              //业务类型
    private String oPhone;          //客户手机
    private Date oDate;              //预约时间
    private int tid;                //站点id
    private int oDelete;            //删除标志
    private String yName;           //类别名字

    public String getzName() {
        return zName;
    }

    public void setzName(String zName) {
        this.zName = zName;
    }

    private String zName;           //站点名字
    private String openId;          //用户标识
    private String oSituation;      //处理情况

    public String getoSituation() {
        return oSituation;
    }

    public void setoSituation(String oSituation) {
        this.oSituation = oSituation;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getyName() {
        return yName;
    }

    public void setyName(String yName) {
        this.yName = yName;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public int getoType() {
        return oType;
    }

    public void setoType(int oType) {
        this.oType = oType;
    }

    public String getoPhone() {
        return oPhone;
    }

    public void setoPhone(String oPhone) {
        this.oPhone = oPhone;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getoDelete() {
        return oDelete;
    }

    public void setoDelete(int oDelete) {
        this.oDelete = oDelete;
    }
}
