package map.demo.entity;

import java.util.Date;

public class Patrol {
    private int pid;
    private String pinfo;
    private String pimg;
    private Date pdate;
    private int zid;
    private int pdelete;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPinfo() {
        return pinfo;
    }

    public void setPinfo(String pinfo) {
        this.pinfo = pinfo;
    }

    public String getPimg() {
        return pimg;
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public Date getPdate() {
        return pdate;
    }

    public void setPdate(Date pdate) {
        this.pdate = pdate;
    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public int getPdelete() {
        return pdelete;
    }

    public void setPdelete(int pdelete) {
        this.pdelete = pdelete;
    }
}
