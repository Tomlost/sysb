package map.demo.entity;

public class Pfenlei {
    private int id;         //金融产品id
    private String flname;  //金融产品分类

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }
}
