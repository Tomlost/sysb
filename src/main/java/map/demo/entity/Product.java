package map.demo.entity;

public class Product {
    private int pid;            //产品id
    private String pname;       //产品名称
    private String cid;         //单位id
    private String lv;          //利率
    private String allowtime;   //最大时长
    private String details;     //详情
    private int flid;           //产品分类id
    private String flname;      //分类名称
    private String cpname;      //网点名称
    private String gjz;         //六站关键字
    private String plimit;         //产品额度


    public String getPlimit() {
        return plimit;
    }

    public void setPlimit(String plimit) {
        this.plimit = plimit;
    }

    public String getGjz() {
        return gjz;
    }

    public void setGjz(String gjz) {
        this.gjz = gjz;
    }

    public String getCpname() {
        return cpname;
    }

    public void setCpname(String cpname) {
        this.cpname = cpname;
    }

    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getLv() {
        return lv;
    }

    public void setLv(String lv) {
        this.lv = lv;
    }

    public String getAllowtime() {
        return allowtime;
    }

    public void setAllowtime(String allowtime) {
        this.allowtime = allowtime;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getFlid() {
        return flid;
    }

    public void setFlid(int flid) {
        this.flid = flid;
    }
}
