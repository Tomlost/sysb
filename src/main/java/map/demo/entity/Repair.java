package map.demo.entity;

import java.util.Date;

public class Repair {
    private int repairId;
    private Date repairTime;
    private String repairInfo;
    private int userId;
    private int repairDel;
    private int repairIdentify;
    private String repairPath;

    public int getRepairId() {
        return repairId;
    }

    public void setRepairId(int repairId) {
        this.repairId = repairId;
    }

    public Date getRepairTime() {
        return repairTime;
    }

    public void setRepairTime(Date repairTime) {
        this.repairTime = repairTime;
    }

    public String getRepairInfo() {
        return repairInfo;
    }

    public void setRepairInfo(String repairInfo) {
        this.repairInfo = repairInfo;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRepairDel() {
        return repairDel;
    }

    public void setRepairDel(int repairDel) {
        this.repairDel = repairDel;
    }

    public int getRepairIdentify() {
        return repairIdentify;
    }

    public void setRepairIdentify(int repairIdentify) {
        this.repairIdentify = repairIdentify;
    }

    public String getRepairPath() {
        return repairPath;
    }

    public void setRepairPath(String repairPath) {
        this.repairPath = repairPath;
    }
}
