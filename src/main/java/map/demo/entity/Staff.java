package map.demo.entity;
public class Staff {
    private int staffId;
    private String staffName;
    private String staffTel;
    private String staffAddress;
    private int positionId;
    private String staffDel;

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffTel() {
        return staffTel;
    }

    public void setStaffTel(String staffTel) {
        this.staffTel = staffTel;
    }

    public String getStaffAddress() {
        return staffAddress;
    }

    public void setStaffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getStaffDel() {
        return staffDel;
    }

    public void setStaffDel(String staffDel) {
        this.staffDel = staffDel;
    }
}
