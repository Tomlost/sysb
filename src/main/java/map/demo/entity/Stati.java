package map.demo.entity;

public class Stati {
    private int yySum;  //预约数
    private int ywSum;  //预约完成数
    private int zdSum;  //站点数
    private int fwSum;  //访问数

    public int getYySum() {
        return yySum;
    }

    public void setYySum(int yySum) {
        this.yySum = yySum;
    }

    public int getYwSum() {
        return ywSum;
    }

    public void setYwSum(int ywSum) {
        this.ywSum = ywSum;
    }

    public int getZdSum() {
        return zdSum;
    }

    public void setZdSum(int zdSum) {
        this.zdSum = zdSum;
    }

    public int getFwSum() {
        return fwSum;
    }

    public void setFwSum(int fwSum) {
        this.fwSum = fwSum;
    }
}
