package map.demo.entity;

import java.util.Date;

public class UKeep {
    private int uid;        //id
    private String uinfo;   //内容
    private String uimg;    //图片
    private int zid;        //网点id
    private int udelete;    //删除标识
    private Date udate;     //上传日期


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUinfo() {
        return uinfo;
    }

    public void setUinfo(String uinfo) {
        this.uinfo = uinfo;
    }

    public String getUimg() {
        return uimg;
    }

    public void setUimg(String uimg) {
        this.uimg = uimg;
    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public int getUdelete() {
        return udelete;
    }

    public void setUdelete(int udelete) {
        this.udelete = udelete;
    }

    public Date getUdate() {
        return udate;
    }

    public void setUdate(Date udate) {
        this.udate = udate;
    }
}
