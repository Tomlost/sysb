package map.demo.entity;

import java.io.Serializable;

public class User implements Serializable {
    private int userId;
    private String uname;
    private String userTel;
    private String userX;
    private String UserDel;
    private String UserPwd;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUserX() {
        return userX;
    }

    public void setUserX(String userX) {
        this.userX = userX;
    }

    public String getUserDel() {
        return UserDel;
    }

    public void setUserDel(String userDel) {
        UserDel = userDel;
    }

    public String getUserPwd() {
        return UserPwd;
    }

    public void setUserPwd(String userPwd) {
        this.UserPwd = userPwd;
    }
}
