package map.demo.entity;

public class WdAdmin {
    private int wid;   //id
    private String wphone; //手机号码
    private int zid;    //网点id
    private int wdelete; //删除标识

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public int getWdelete() {
        return wdelete;
    }

    public void setWdelete(int wdelete) {
        this.wdelete = wdelete;
    }

    public String getWphone() {
        return wphone;
    }

    public void setWphone(String wphone) {
        this.wphone = wphone;
    }


}
