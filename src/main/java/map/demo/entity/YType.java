package map.demo.entity;

public class YType {
    private int yid;        //id
    private String yName;   //类型名称
    private int yDelete;    //删除标识

    public int getYid() {
        return yid;
    }

    public void setYid(int yid) {
        this.yid = yid;
    }

    public String getyName() {
        return yName;
    }

    public void setyName(String yName) {
        this.yName = yName;
    }

    public int getyDelete() {
        return yDelete;
    }

    public void setyDelete(int yDelete) {
        this.yDelete = yDelete;
    }
}
