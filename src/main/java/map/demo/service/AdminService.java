package map.demo.service;

import  map.demo.entity.Admin;

public interface AdminService {
    Admin loginAdmin(String adminName,String adminPwd);
}
