package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Charge;
import  map.demo.entity.Complain;

public interface ChargeService {
    PageInfo<Charge> getAllCharge(int pageNum, int pageSize);

    boolean addCharge(Charge charge);
}
