package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Complain;

import java.util.List;

public interface ComplainService {
    PageInfo<Complain> AllCom(int pageNum, int pageSize);

    PageInfo<Complain> getAllCom(int pageNum, int pageSize,int userId);

    PageInfo<Complain> getNoCom(int pageNum, int pageSize,int userId);

    PageInfo<Complain> getYesCom(int pageNum, int pageSize,int userId);

    boolean addCom(Complain complain);

    Complain showCom(int complainId);

    boolean update(int complainId);
}
