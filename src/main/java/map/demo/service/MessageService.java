package map.demo.service;

import map.demo.entity.Message;

import java.util.List;

public interface MessageService {
    List<Message> getMessageList();

    Message getMessageByCid(int cid);

    List<Message> getMessageListByPower();
}
