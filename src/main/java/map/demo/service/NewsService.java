package map.demo.service;
import  com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageInfo;
import map.demo.entity.News;

import org.apache.ibatis.annotations.Param;

import java.util.List;
public interface NewsService {


    PageInfo<News> getAllNewsList(int pageNum, int pageSize);

    PageInfo<News> getAllNewsListByFenLei(int pageNum, int pageSize,News news);

    PageInfo<News> getAllNewsListByZd(int pageNum, int pageSize);

    News showNewsById(int id);

    PageInfo<News> showNewsBytitle(int pageNum, int pageSize,String title);

    boolean updateNewsInfo(News news);

    boolean addNews(News news);

    boolean deleteNews(int id);

    boolean editzhiding(int id);

    boolean editnozhiding(int id);

    boolean editVisible(int id);

    boolean editnoVisible(int id);



}

