package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Complain;
import  map.demo.entity.Notice;

import java.util.List;

public interface NoticeService {
    PageInfo<Notice> showAllNotice(int pageNum, int pageSize);

    Notice show(int id);
}
