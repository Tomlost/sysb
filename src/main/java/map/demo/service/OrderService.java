package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Order;

public interface OrderService {
    PageInfo<Order> getAllOrderList(int pageNum,int pageSize);
    PageInfo<Order> getAllOrder(int pageNum,int pageSize);

    PageInfo<Order> getAllwclOrder(int pageNum,int pageSize);
    PageInfo<Order> getOrderByTid(int pageNum,int pageSize,int tid);

    boolean addOrder(Order order);

    boolean delOrder(int oid,String oSituation);
    boolean updateoder(int oid);
    PageInfo<Order> getOrderByOpenId(int pageNum,int pageSize,String openId);
}
