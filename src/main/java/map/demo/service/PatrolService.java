package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Patrol;

public interface PatrolService {
    boolean addPatrol(Patrol patrol);

    PageInfo<Patrol> getAllPatrol(int pageNum, int pageSize);

    boolean delPatrol(int pid);
}
