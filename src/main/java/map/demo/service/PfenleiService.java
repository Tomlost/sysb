package map.demo.service;

import map.demo.entity.Pfenlei;

import java.util.List;

public interface PfenleiService {
    List<Pfenlei> getAllPf();
}
