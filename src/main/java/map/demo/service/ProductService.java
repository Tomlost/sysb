package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.Product;

import java.util.List;

public interface ProductService {
    PageInfo<Product> getAllProByFl(Product product);

    Product getProByPid(int pid);
}
