package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Repair;

import java.util.List;

public interface RepairService {
    boolean addRepair(Repair repair);

    boolean updateRepair(Repair repair);

    PageInfo<Repair> showR(int pageNum,int pageSize);
}
