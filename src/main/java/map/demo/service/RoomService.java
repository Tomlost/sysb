package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Room;

import java.util.List;

public interface RoomService {

    PageInfo<Room> allRoom(int uid,int pageNum,int pageSize);

    boolean updateRoom(int uid,int id);
}
