package map.demo.service;

import com.github.pagehelper.PageInfo;
import  map.demo.entity.Staff;

import java.util.List;

public interface StaffService {
    PageInfo<Staff> staffAll(int pageSize,int pageNum);

    boolean updateStaff(Staff staff);

    boolean addStaff(Staff staff);

    boolean deleteStaff(int staffId);

    Staff showStaff(int staffId);
}
