package map.demo.service;

import map.demo.entity.Stati;

public interface StatiService {

    Stati getAllSta();

    boolean upYySum();

    boolean upYwSum();

    boolean upZdSum();

    boolean upFwSum();
}
