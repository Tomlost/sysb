package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.UKeep;

public interface UKeepService {
    boolean addUKeep(UKeep uKeep);

    PageInfo<UKeep> getAllUKeep(int pageNum,int pageSize);

    boolean delUKeep(int uid);
}
