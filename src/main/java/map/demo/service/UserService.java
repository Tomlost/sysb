package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    User login(String userTel,String userPwd);

    boolean update(String userPwd,Integer userId);

    PageInfo<User> userShow(int pageNum,int pageSize);

    User showUserById(int userId);

    boolean updateUser(User user);

    boolean addUser(User user);

    boolean deleteUser(int userId);

    List<User> showUserState();

    boolean updateUserState(int userId);
}
