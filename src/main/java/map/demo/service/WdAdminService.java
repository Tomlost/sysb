package map.demo.service;

import com.github.pagehelper.PageInfo;
import map.demo.entity.WdAdmin;

public interface WdAdminService {
    //登陆
    WdAdmin loginWdAdmin(String wphone);

    PageInfo<WdAdmin> getAllWdAdminList(int pageNum,int pageSize);

    PageInfo<WdAdmin> getWdAdminListByZid(int pageNum,int pageSize,int zid);

    boolean addWdAdmin(WdAdmin wdAdmin);

    boolean delWdAdmin(int wid);
}
