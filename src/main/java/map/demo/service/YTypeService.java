package map.demo.service;

import map.demo.entity.YType;

import java.util.List;

public interface YTypeService {
    YType getYTypeByYid(int yid);

    List<YType> getAllYTypeList();
}
