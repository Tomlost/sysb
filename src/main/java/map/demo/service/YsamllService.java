package map.demo.service;

import map.demo.entity.Ysamll;

import java.util.List;

public interface YsamllService {
    List<Ysamll> getAllYs();

    boolean updateYs(Ysamll ysamll);
}
