package map.demo.service.impl;

import map.demo.dao.AdminMapper;
import map.demo.entity.Admin;
import map.demo.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin loginAdmin(String adminName, String adminPwd) {
        return adminMapper.loginAdmin(adminName,adminPwd);
    }
}
