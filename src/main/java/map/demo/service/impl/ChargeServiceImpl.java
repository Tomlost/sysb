package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.ChargeMapper;
import map.demo.entity.Charge;
import map.demo.entity.Complain;
import map.demo.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChargeServiceImpl implements ChargeService {
    @Autowired
    private ChargeMapper chargeMapper;

    @Override
    public PageInfo<Charge> getAllCharge(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Charge> complainList = chargeMapper.getAllCharge();
        return new PageInfo<>(complainList);
    }

    @Override
    public boolean addCharge(Charge charge) {
        if (chargeMapper.addCharge(charge)>0){
            return true;
        }
        return false;
    }
}
