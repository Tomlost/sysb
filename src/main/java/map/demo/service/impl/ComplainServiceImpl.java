package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.ComplainMapper;
import map.demo.entity.Complain;
import map.demo.service.ComplainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ComplainServiceImpl implements ComplainService {
    @Autowired
    private ComplainMapper complainMapper;

    @Override
    public PageInfo<Complain> AllCom(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Complain> complainList = complainMapper.AllCom();
        return new PageInfo<>(complainList);
    }

    @Override
    public PageInfo<Complain> getAllCom(int pageNum, int pageSize, int userId) {
        PageHelper.startPage(pageNum,pageSize);
        List<Complain> complainList = complainMapper.getAllCom(userId);
        return new PageInfo<>(complainList);
    }

    @Override
    public PageInfo<Complain> getNoCom(int pageNum, int pageSize,int userId) {
        PageHelper.startPage(pageNum,pageSize);
        List<Complain> complainList = complainMapper.getNoCom(userId);
        return new PageInfo<>(complainList);
    }

    @Override
    public PageInfo<Complain> getYesCom(int pageNum, int pageSize,int userId) {
        PageHelper.startPage(pageNum,pageSize);
        List<Complain> complainList = complainMapper.getYesCom(userId);
        return new PageInfo<>(complainList);
    }

    @Override
    public boolean addCom(Complain complain) {
        if (complainMapper.addCom(complain)>0){
            return true;
        }
        return false;
    }

    @Override
    public Complain showCom(int complainId) {
        return complainMapper.showCom(complainId);
    }

    @Override
    public boolean update(int complainId) {
        if (complainMapper.updateCom(complainId)>0){
            return true;
        }
        return false;
    }
}
