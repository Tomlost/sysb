package map.demo.service.impl;

import map.demo.dao.MessageMapper;
import map.demo.entity.Message;
import map.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageMapper messageMapper;

    @Override
    public List<Message> getMessageList(){
       List<Message> messageList =  messageMapper.getMessageList();
        return messageList;
    }

    @Override
    public Message getMessageByCid(int cid) {
        return messageMapper.getMessageByCid(cid);
    }

    @Override
    public List<Message> getMessageListByPower() {
        return messageMapper.getMessageListByPower();
    }
}
