package map.demo.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.NewsMapper;
import map.demo.entity.News;
import map.demo.utils.Result;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;


import map.demo.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService{

    @Autowired
    private NewsMapper NewsMapper;


    @Override
    public PageInfo<News> getAllNewsList(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<News> newsList = NewsMapper.getAllNewsList();
        return new PageInfo<>(newsList);
    }

    @Override
    public PageInfo<News> getAllNewsListByFenLei(int pageNum, int pageSize,News news) {
        PageHelper.startPage(pageNum,pageSize);
        List<News> newsList = NewsMapper.getAllNewsListByFenLei(news);
        return new PageInfo<>(newsList);
    }

    @Override
    public PageInfo<News> getAllNewsListByZd(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<News> newsList = NewsMapper.getAllNewsListByZd();
        return new PageInfo<>(newsList);
    }

    @Override
    public News showNewsById(int id) {

        News newsList = NewsMapper.showNewsById(id);
        return newsList;
    }
    @Override
    public PageInfo<News> showNewsBytitle(int pageNum, int pageSize,String title) {
        PageHelper.startPage(pageNum,pageSize);
        List<News> newsList = NewsMapper.showNewsBytitle(title);
        return new PageInfo<>(newsList);

    }
    @Override
    public boolean updateNewsInfo(News news) {
        if (NewsMapper.updateNewsInfo(news)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean addNews(News news) {
        if (NewsMapper.addNews(news)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteNews(int id) {
        if (NewsMapper.deleteNews(id)>0){
            return true;
        }
        return false;
    }
     @Override
     public boolean editzhiding(int id) {
         if (NewsMapper.editzhiding(id)>0){
             return true;
         }
         return false;
     }
    @Override
    public boolean editnozhiding(int id) {
        if (NewsMapper.editnozhiding(id)>0){
            return true;
        }
        return false;
    }
    @Override
    public boolean editVisible(int id) {
        if (NewsMapper.editVisible(id)>0){
            return true;
        }
        return false;
    }
    @Override
    public boolean editnoVisible(int id) {
        if (NewsMapper.editnoVisible(id)>0){
            return true;
        }
        return false;
    }




}
