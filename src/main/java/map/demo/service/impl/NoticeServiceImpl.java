package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.NoticeMapper;
import map.demo.entity.Complain;
import map.demo.entity.Notice;
import map.demo.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public PageInfo<Notice> showAllNotice(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Notice> list = noticeMapper.getNoticeList();
           return new PageInfo<>(list);
    }

    @Override
    public Notice show(int id) {
        return noticeMapper.getNotice(id);
    }
}
