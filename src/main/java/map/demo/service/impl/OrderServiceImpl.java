package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.OrderMapper;
import map.demo.entity.Order;
import map.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public PageInfo<Order> getAllOrderList(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList = orderMapper.getAllOrderList();
        return new PageInfo<>(orderList);
    }
    @Override
    public PageInfo<Order> getAllOrder(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList = orderMapper.getAllOrder();
        return new PageInfo<>(orderList);
    }

    @Override
    public PageInfo<Order> getAllwclOrder(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList = orderMapper.getAllwclOrder();
        return new PageInfo<>(orderList);
    }

    @Override
    public PageInfo<Order> getOrderByTid(int pageNum,int pageSize,int tid) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderByIdList = orderMapper.getOrderByTid(tid);
        return new PageInfo<>(orderByIdList);
    }

    @Override
    public boolean addOrder(Order order) {
        if (orderMapper.addOrder(order)>0){
            return true;
        }
        return false;
    }
    @Override
    public boolean updateoder(int oid) {
        if (orderMapper.updateoder(oid)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean delOrder(int oid,String oSituation) {
        if (orderMapper.delOrder(oid,oSituation)>0){
            return true;
        }
        return false;
    }

    @Override
    public PageInfo<Order> getOrderByOpenId(int pageNum, int pageSize, String openId) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderByIdList = orderMapper.getOrderByOpenId(openId);
        return new PageInfo<>(orderByIdList);
    }
}
