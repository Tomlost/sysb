package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.PatrolMapper;
import map.demo.entity.Patrol;
import map.demo.service.PatrolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatrolServiceImpl implements PatrolService {
    @Autowired
    private PatrolMapper patrolMapper;


    @Override
    public boolean addPatrol(Patrol patrol) {
        if (patrolMapper.addPatrol(patrol)>0){
            return true;
        }
        return false;
    }

    @Override
    public PageInfo<Patrol> getAllPatrol(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Patrol> patrolList = patrolMapper.getAllPatrol();
        return new PageInfo<>(patrolList);
    }

    @Override
    public boolean delPatrol(int pid) {
        if (patrolMapper.delPatrol(pid)>0){
            return true;
        }
        return false;
    }
}
