package map.demo.service.impl;

import map.demo.dao.PfenleiMapper;
import map.demo.entity.Pfenlei;
import map.demo.service.PfenleiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PfenleiServiceImpl implements PfenleiService {
    @Autowired
    private PfenleiMapper pfenleiMapper;

    @Override
    public List<Pfenlei> getAllPf() {
        return pfenleiMapper.getAllPf();
    }
}
