package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.ProductMapper;
import map.demo.entity.Patrol;
import map.demo.entity.Product;
import map.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public PageInfo<Product> getAllProByFl(Product product) {
        PageHelper.startPage(1000,1);
        List<Product> productList = productMapper.getAllProByFl(product);
        return new PageInfo<>(productList);
    }

    @Override
    public Product getProByPid(int pid) {
        return productMapper.getProByPid(pid);
    }
}
