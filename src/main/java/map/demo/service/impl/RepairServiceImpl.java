package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.RepairMapper;
import map.demo.entity.Repair;
import map.demo.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairMapper repairMapper;


    @Override
    public boolean addRepair(Repair repair) {
        if (repairMapper.addRepair(repair)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateRepair(Repair repair) {
        if (repairMapper.updateRepair(repair)>0){
            return true;
        }
        return false;
    }

    @Override
    public PageInfo<Repair> showR(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Repair> repairList = repairMapper.showR();
        return new PageInfo<>(repairList);
    }
}
