package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.RoomMapper;
import map.demo.entity.Repair;
import map.demo.entity.Room;
import map.demo.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomMapper mapper;

    @Override
    public PageInfo<Room> allRoom(int uid,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Room> repairList = mapper.allRoom(uid);
        return new PageInfo<>(repairList);
    }

    @Override
    public boolean updateRoom(int uid, int id) {
        if (mapper.updateRoom(uid,id)>0){
            return true;
        }
        return false;
    }
}
