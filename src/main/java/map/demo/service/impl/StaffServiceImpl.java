package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.StaffMapper;
import map.demo.entity.Staff;
import map.demo.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffMapper staffMapper;

    @Override
    public PageInfo<Staff> staffAll(int pageSize,int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        List<Staff> staffList = staffMapper.staffAll();
        return new PageInfo<>(staffList);
    }

    @Override
    public boolean updateStaff(Staff staff) {
        return false;
    }

    @Override
    public boolean addStaff(Staff staff) {
        if (staffMapper.addStaff(staff)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteStaff(int staffId) {
        if (staffMapper.deleteStaff(staffId)>0){
            return true;
        }
        return false;
    }

    @Override
    public Staff showStaff(int staffId) {
        return null;
    }
}
