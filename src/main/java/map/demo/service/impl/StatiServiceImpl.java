package map.demo.service.impl;

import map.demo.dao.StatiMapper;
import map.demo.entity.Stati;
import map.demo.service.StatiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatiServiceImpl implements StatiService {
    @Autowired
    private StatiMapper statiMapper;

    @Override
    public Stati getAllSta() {
        return statiMapper.getAllSta();
    }

    @Override
    public boolean upYySum() {
        if (statiMapper.upYySum()>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean upYwSum() {
        if (statiMapper.upYwSum()>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean upZdSum() {
        if (statiMapper.upZdSum()>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean upFwSum() {
        if (statiMapper.upFwSum()>0){
            return true;
        }
        return false;
    }
}
