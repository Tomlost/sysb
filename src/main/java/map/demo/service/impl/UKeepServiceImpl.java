package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.UKeepMapper;
import map.demo.entity.UKeep;
import map.demo.service.UKeepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UKeepServiceImpl implements UKeepService {
    @Autowired
    private UKeepMapper uKeepMapper;

    @Override
    public boolean addUKeep(UKeep uKeep) {
        if (uKeepMapper.addUKeep(uKeep)>0){
            return true;
        }
        return false;
    }

    @Override
    public PageInfo<UKeep> getAllUKeep(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<UKeep> uKeepList = uKeepMapper.getAllUKeep();
        return new PageInfo<>(uKeepList);
    }

    @Override
    public boolean delUKeep(int uid) {
        if (uKeepMapper.delUKeep(uid)>0){
            return true;
        }
        return false;
    }
}
