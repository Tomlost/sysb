package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.UserMapper;
import map.demo.entity.User;
import map.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public User login(String userTel, String userPwd) {
        return userMapper.login(userTel,userPwd);
    }

    @Override
    public boolean update(String userPwd,Integer userId) {
        if (userMapper.update(userPwd,userId)>0){
            return true;
        }
        return false;
    }

    @Override
    public PageInfo<User> userShow(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<User> userList = userMapper.showUser();
        return new PageInfo<>(userList);
    }

    @Override
    public User showUserById(int userId) {

        User userList = userMapper.showUserById(userId);
        return userList;
    }

    @Override
    public boolean updateUser(User user) {
        if (userMapper.updateUser(user)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean addUser(User user) {
        if (userMapper.addUser(user)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteUser(int userId) {
        if (userMapper.deleteUser(userId)>0){
            return true;
        }
        return false;
    }

    @Override
    public List<User> showUserState() {
        return userMapper.showUserState();
    }

    @Override
    public boolean updateUserState(int userId) {
        if (userMapper.updateUserState(userId)>0){
            return true;
        }
        return false;
    }
}
