package map.demo.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import map.demo.dao.WdAdminMapper;
import map.demo.entity.WdAdmin;
import map.demo.service.WdAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WdAdminServiceImpl implements WdAdminService {
    @Autowired
    private WdAdminMapper wdAdminMapper;


    @Override
    public WdAdmin loginWdAdmin(String wphone) {
        return wdAdminMapper.loginWdAdmin(wphone);
    }

    @Override
    public PageInfo<WdAdmin> getAllWdAdminList(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<WdAdmin> wdAdminList = wdAdminMapper.getAllWdAdminList();
        return new PageInfo<>(wdAdminList);
    }

    @Override
    public PageInfo<WdAdmin> getWdAdminListByZid(int pageNum,int pageSize,int zid) {
        PageHelper.startPage(pageNum,pageSize);
        List<WdAdmin> wdAdminList = wdAdminMapper.getWdAdminListByZid(zid);
        return new PageInfo<>(wdAdminList);
    }

    @Override
    public boolean addWdAdmin(WdAdmin wdAdmin) {
        if (wdAdminMapper.addWdAdmin(wdAdmin)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean delWdAdmin(int wid) {
        if (wdAdminMapper.delWdAdmin(wid)>0){
            return true;
        }
        return false;
    }

}
