package map.demo.service.impl;

import map.demo.dao.YTypeMapper;
import map.demo.entity.YType;
import map.demo.service.YTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YTypeServiceImpl implements YTypeService{
    @Autowired
    private YTypeMapper yTypeMapper;


    @Override
    public YType getYTypeByYid(int yid) {
        return yTypeMapper.getYTypeByYid(yid);
    }

    @Override
    public List<YType> getAllYTypeList() {
        return yTypeMapper.getAllYTypeList();
    }


}
