package map.demo.service.impl;

import map.demo.dao.YsamllMapper;
import map.demo.entity.Ysamll;
import map.demo.service.YsamllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YsamllServiceImpl implements YsamllService {
    @Autowired
    private YsamllMapper ysamllMapper;

    @Override
    public List<Ysamll> getAllYs() {
        return ysamllMapper.getAllYs();
    }

    @Override
    public boolean updateYs(Ysamll ysamll) {
        if (ysamllMapper.updateYs(ysamll)>0){
            return true;
        }
        return false;
    }
}
