package map.demo.utils;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
@Mapper
public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
//    @Value("${pay.aliypay.app_id}")
    private String app_id = "2021000118656864";

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }
    
    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCNxG4t+ycovOuUB6g1qzEUnsUxwlUt/a4NAiRN1aIlfocF9A9G/FdgNy1RUq2L+K3+siyxboFRrhuyqPTA9W57u/IBx0Nuqgd3uElzAcAqkpeTGx9OPBUSL10OvFcAybacYXK1kDKllHAgnEkZfubmqIdz7ZaOo+ls7dIuw8WK+gMjh632sXjVoamwFzDEyqkO3f2oh/O0wCxOqFZ7a56bh4OvW6DVdvZWAmqHNA+gifNXPqTVzq/YUBJ5kBAbED1ULirVSEHqJbkVZ7AJDVv4r5LrQuKoPHS3dCs8KNv5es4inK526/5QYSAlOq0FwLniD3+DHtxMR0wTM2jcNeqDAgMBAAECggEAWSPCA7eAD+V6W0HVSqmx2r1aUMdYt+pinltSaH6Dzf+Y9u1XDdNtPVp0bEYISZK8FQIDiC1rsug/Ej+aZhnstnCe4u9gPx06RNTjmiTnp9004ZKURKUSurR9Vt6l9iDfacT8U52vut1zA45BiH56wV4GjaY+nMID7DI9jpoahB9v67Nfb4iWwWHmv+vAPerqmdYlGdcvbTQ5sjPkWb5AjLw3jv5ULdwzDM7iA+tiUMUnLhXG4fnNxJl24N4+0Rm/U5Z1lC7sLop6zqbrNw9xpZaRMQxEowdtDgguxeZCMGu6zomXdMWNPS7LaCZ2izEj+R6/nu7jFL8rxIUmtCwzQQKBgQDLx32P87QKjYD8x+wbgZKywcxL1OIVCK6G2TcxPt6LZFXbolTtwVQIZfinyeX1zTwamQqi3ouj73yj5RGox3/U2d7dKY5gKQzL0M8DKqFpvhdR2u6SVFRGkYr4gOH0yhCm1p8xGnUkLEu0DkOScKwEhJdIB62WTYRcIidtf9mrZwKBgQCyGMa0rmbnIz8wmSGW6sjIM1IDCP+E42k5Eyb0JgVul/pqAaiAAvzna7omax5zTsKKWO9xGDVIDieZwQrBfBC6f3FKUxWsfhx/LW0HIorngydx3uUAkKnoI0tGObrZuZdPVxhwZl97JiVfOWUlTpwLVJszRa1llMn9yOJdHFRyhQKBgQC4nPFfMczhZpL9720y+i9Z7QLVjHaJ0cSzfCRpY+if5fV2tlk00Fn+BcvAzsOb7SRPgGjHZtChYzzLCqL2Ak8IGZ2zRYusCGSEXZa96qgzMTjVOn+6Z9W13PNG8RRD/bAaRgQQq0SG0e5/oBCNoa/lo+2eHv6fVXjdMz//Dr23bwKBgGFUeQO1bDdVY+tDRMAdf60pnuBZwor1XUP724YCYWWXWRsl3NhYya4PiaQFVpbdrCO/htvfxv6h/bJhdWdYTiweCeP8lBO4uAoo4TTNA+ySGLR6g9xTMwfSggjW8+6ZJsANcSW8TOIYbsQjscs5sr3edxMGOUUSk3FGn68TFHOhAoGAHiGR+Cy2FbX1tZi/GTluoduAEcHLWDX+m7Hrrg/ezPfMvjInk784nI9z0VXSsXchGxLnqrSyJpk11V/UnseRsnGPAIJs/lBLop/8HgldNBwWRSyJFBbmDJuAzaRYgRXFOpfjRhsBc/+sAmgCkqKzWiYGug8jo2EAUxJzCIl9ALU=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkLito+dKvW8kSZDj6bKYHCoksCzTrqEuUvxSqsnd5Qq5P0riyoPxfGDr4LM3eazd7xNjfgebAxaiM09YJ8+glNSITUMJaboEGl755eYPrGRfbL09qu5vs6onbNPoHxnL77MBPJ49MJqRibScv4jx32FQkoi+xLcRwa0ZdVcIDAvrCMDYiWHG58yKVoEteWY3yXl1oFGYkqzllBYJi/8UlmBEgzjuuQWUPkrRrTFYulMqMr2Uk2zlRVKXRGe3swiM4xXVJXs34Jp1mwQkXQ+C0DjLrR/cm0Qm6mDbpUniEcnU/ZBBOW+iJr/0t7LifitMaqkD1vr8E4bkA1kBW4hr5wIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://myfzux.natappfree.cc/zfb/notify_url";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "https://www.baidu.com";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
//    public static String log_path = "C:\\";
}
