package map.demo.utils;


/**
 * 定义项目使用的常量
 */

public class Constants {
    //public final static String FILE_UPLOAD_DIC = "/opt/image/upload/";//上传文件的默认url前缀，根据部署设置自行修改
    //public final static String FILE_UPLOAD_DIC = "C:/Users/Administrator/IdeaProjects/sysb/src/main/resources/static/uploadFile"; // 上传文件的默认url前缀，根据部署设置自行修改
    public final static String FILE_UPLOAD_DIC = "c:/Users/Administrator/IdeaProjects/sysb/upload/";
    public final static String USER_SESSION_KEY = "id"; // session中user的key

    public final static String VERIFY_CODE_KEY = "userVerifyCode"; // 验证码key


}
